# rlimit nofile 524288

# Use user-specific tmpdir
# ES_TMPDIR=/var/tmp/elasticsearch

# Required env
ES_HOME=@ES_HOMEDIR@
ES_PATH_CONF=/etc/elasticsearch

service [2345] name:elasticsearch \
    pid:elasticsearch \
    @@ES_USER@:@ES_GROUP@ \
    <run/mount-late/success> \
    /usr/share/elasticsearch/bin/elasticsearch --quiet \
    -- Elasticsearch service
