for i in halt shutdown; do
  if ! readlink /sbin/$i | grep -q 'initctl'; then
    if file /sbin/$i | grep -q ELF; then
      mv /sbin/$i /sbin/${i}.sysv
      logger -s -p user.warn "FINIT: Renamed /sbin/${i} to /sbin/${i}.sysv"
    fi
    rm -f /sbin/$i
    ln -sf initctl /sbin/${i}
    logger -s -p user.warn "FINIT: Symlinked /sbin/${i} to /sbin/initctl"
  fi
done

for i in reboot poweroff suspend; do
  if ! readlink /sbin/$i | grep -q halt; then
    if file /sbin/$i | grep -q ELF; then
      mv /sbin/$i /sbin/${i}.sysv
      logger -s -p user.warn "FINIT: Renamed /sbin/${i} to /sbin/${i}.sysv"
    fi
    rm -f /sbin/$i
    ln -sf halt /sbin/$i
    logger -s -p user.warn "FINIT: Symlinked /sbin/${i} to /sbin/halt"
  fi
done

if ! readlink /sbin/init | grep -q finit; then
  if file /sbin/init | grep -q ELF; then
    mv /sbin/init /sbin/init.sysv
    logger -s -p user.warn "FINIT: Renamed /sbin/init to /sbin/init.sysv"
  fi
  rm -f /sbin/init
  ln -sf finit /sbin/init
  logger -s -p user.warn "FINIT: Symlinked /sbin/init to /sbin/finit"
fi

