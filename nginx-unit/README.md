# NGINX Unit - universal web app server


NGINX Unit is a lightweight and versatile open source server that simplifies the application stack by natively executing application code across eight different programming language runtimes.

WWW: https://unit.nginx.org/

# Disable PHP Apache SAPI
To build PHP module you **MUST REBUILD** PHP package without Apache apxs (delete this line: `--with-apxs2=/usr/bin/apxs`) and enable embed library support: `--enable-embed`.

# QUICTLS support
To build with QUICTLS support, install QUICTLS library and use this switch
  QUICTLS_ENABLE=YES ./nginx-unit.SlackBuild
