#!/bin/sh
config() {
  NEW="$1"
  OLD="`dirname $NEW`/`basename $NEW .new`"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "`cat $OLD | md5sum`" = "`cat $NEW | md5sum`" ]; then # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

# Check for elasticsearch user and group availability
ES_GROUP=@ES_GROUP@
ES_GID=@ES_GID@
ES_USER=@ES_USER@
ES_UID=@ES_UID@
if ! getent group $ES_GROUP 2>&1 > /dev/null; then
  echo "Add group $ES_GROUP with gid $ES_GID"
  groupadd -r -g $ES_GID $ES_GROUP;
fi
if ! getent passwd $ES_USER 2>&1 > /dev/null; then
  echo "Add user $ES_USER ($ES_UID) with group $ES_GROUP ($ES_GID)"
  useradd -r -u $ES_UID -g $ES_GROUP -d @ES_HOMEDIR@ -s /bin/bash -c "Elasticsearch Daemon" $ES_USER;
else
  echo "Using existing user $ES_USER ($ES_UID) and group $ES_GROUP ($ES_GID)"
fi

config etc/elasticsearch/elasticsearch-plugins.example.yml.new
config etc/elasticsearch/elasticsearch.yml.new
config etc/elasticsearch/jvm.options.new
config etc/elasticsearch/log4j2.properties.new
config etc/elasticsearch/role_mapping.yml.new
config etc/elasticsearch/roles.yml.new
config etc/elasticsearch/users.new
config etc/elasticsearch/users_roles.new
config etc/rc.d/rc.elasticsearch.new
