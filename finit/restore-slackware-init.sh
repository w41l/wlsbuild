for i in halt init shutdown; do
  if [ -e /sbin/${i}.sysv ]; then
    if file /sbin/${i} | grep -q ELF; then
      BAK="bak-$(date +%s)"
      mv /sbin/${i} /sbin/${i}.${BAK}
      logger -s -p user.warn "FINIT: Renamed /sbin/${i} to /sbin/${i}.${BAK}"
    fi
    rm -f /sbin/$i
    mv /sbin/${i}.sysv /sbin/$i;
    logger -s -p user.warn "FINIT: Replaced /sbin/${i} with /sbin/${i}.sysv"
  fi
done

if ! readlink /sbin/telinit | grep -q 'init'; then
  rm -f /sbin/telinit
  ln -sf init /sbin/telinit;
  logger -s -p user.warn "FINIT: Symlinked /sbin/telinit to /sbin/init"
fi

for i in poweroff reboot suspend; do
  if ! readlink /sbin/$i | grep -q 'halt'; then
    if file /sbin/${i} | grep -q ELF; then
      BAK="bak-$(date +%s)"
      mv /sbin/${i} /sbin/${i}.${BAK}
      logger -s -p user.warn "FINIT: Renamed /sbin/${i} to /sbin/${i}.${BAK}"
    fi
    rm -f /sbin/$i
    ln -sf halt /sbin/$i;
    logger -s -p user.warn "FINIT: Symlinked /sbin/${i} to /sbin/halt"
  fi
done
