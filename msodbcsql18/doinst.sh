
#=============================#
if ! cat /etc/ld.so.conf.d/*.conf 2>/dev/null | grep -q "/opt/microsoft/msodbcsql18/lib64"; then
  echo "Spamming /etc/ld.so.conf.d/msodbcsql18.conf"
  echo "/opt/microsoft/msodbcsql18/lib64" >/etc/ld.so.conf.d/msodbcsql18.conf
  /sbin/ldconfig
fi

echo "Install msodbcsql18 driver with this command (run as root):"
echo "  odbcinst -i -s -f /opt/microsoft/msodbcsql18/etc/odbcinst.ini -l"
echo
