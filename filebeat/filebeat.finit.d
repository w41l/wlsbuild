service [2345] name:filebeat \
    pid:filebeat \
    /usr/sbin/filebeat --path.config /etc/filebeat --path.home /usr/share/filebeat --path.data /var/lib/filebeat --path.logs /var/log/filebeat -c filebeat.yml run \
    -- Elasticsearch filebeat agent
