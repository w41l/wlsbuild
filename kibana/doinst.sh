config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}
preserve_perms() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  if [ -e $OLD ]; then
    cp -a $OLD ${NEW}.incoming
    cat $NEW > ${NEW}.incoming
    mv ${NEW}.incoming $NEW
  fi
  config $NEW
}

# Check for elasticsearch user and group availability
ES_GROUP=@ES_GROUP@
ES_GID=@ES_GID@
ES_USER=@ES_USER@
ES_UID=@ES_UID@
if ! getent group $ES_GROUP 2>&1 > /dev/null; then
  echo "Add group $ES_GROUP with gid $ES_GID"
  groupadd -r -g $ES_GID $ES_GROUP;
fi
if ! getent passwd $ES_USER 2>&1 > /dev/null; then
  echo "Add user $ES_USER ($ES_UID) with group $ES_GROUP ($ES_GID)"
  useradd -r -u $ES_UID -g $ES_GROUP -d @ES_HOMEDIR@ -s /bin/bash -c "Elasticsearch Daemon" $ES_USER;
else
  echo "Using existing user $ES_USER ($ES_UID) and group $ES_GROUP ($ES_GID)"
fi

config etc/kibana/kibana.yml.new
config etc/kibana/node.options.new
preserve_perms etc/rc.d/rc.kibana.new
