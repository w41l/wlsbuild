# Build order

* bubblewrap: https://gitlab.com/w41l/slackbuilds/-/tree/master/system/bubblewrap
* libwpe: https://gitlab.com/w41l/slackbuilds/-/tree/master/libraries/libwpe
* wpewebkit: https://gitlab.com/w41l/slackbuilds/-/tree/master/libraries/wpewebkit
* wpebackend-fdo: https://gitlab.com/w41l/slackbuilds/-/tree/master/libraries/wpebackend-fdo
* webkit2gtk: https://gitlab.com/w41l/slackbuilds/-/tree/master/libraries/webkit2gtk
* gcab: https://gitlab.com/w41l/slackbuilds/-/tree/master/libraries/gcab
* appstream-glib: https://gitlab.com/w41l/slackbuilds/-/tree/master/libraries/appstream-glib
* gjs: https://gitlab.com/w41l/wlsgnome/-/tree/master/base/gjs
* foliate: https://gitlab.com/w41l/wlsbuild/-/tree/master/foliate
