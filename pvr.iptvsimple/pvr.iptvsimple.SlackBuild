#!/bin/sh

# Slackware build script for pvr.iptvsimple

# Copyright 2019  Widya Walesa <walecha99@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. FURTHERMORE I AM NOT LIABLE IF
# YOUR DATA IS DESTROYED, YOUR HOUSE BURNS DOWN OR YOUR DOG RUNS OFF.

PRGNAM=pvr.iptvsimple
eval $(cat $PRGNAM.info | grep "VERSION=")
KODIVER=${KODIVER:-18.2}
RAPIDXMLVER=${RAPIDXMLVER:-1.13}
ZLIBVER=${ZLIBVER:-1.2.11}
CODNAM="Leia"
BUILD=${BUILD:-1}
TAG=${TAG:-_wls}
NUMJOBS=${NUMJOBS:-"-j $(expr $(nproc) \* 3 / 4)"}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -pipe -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -pipe -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -pipe -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/wlsbuild}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

rm -rf $PKG
mkdir -p $WRK $PKG/usr/lib${LIBDIRSUFFIX}/kodi $PKG/usr/share/kodi $OUTPUT
cd $WRK
rm -rf $PRGNAM-$VERSION-$CODNAM
tar xvf $CWD/$PRGNAM-$VERSION-$CODNAM.tar.gz
cd $PRGNAM-$VERSION-$CODNAM
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Don't have the Makefile download internal depends during install.
ln -s $CWD/rapidxml-${RAPIDXMLVER}.zip \
  depends/common/rapidxml/rapidxml-${RAPIDXMLVER}.zip
ln -s $CWD/zlib-${ZLIBVER}.tar.gz \
  depends/common/zlib/zlib-${ZLIBVER}.tar.gz

mkdir build
cd build
  cmake \
    -DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS_RELEASE:STRING="$SLKCFLAGS" \
    -DCMAKE_BUILD_TYPE=Release \
    -DADDONS_TO_BUILD=pvr.iptvsimple \
    -DCMAKE_INSTALL_PREFIX=../../xbmc-$KODIVER-$CODNAM/addons \
    ../../xbmc-$KODIVER-$CODNAM/cmake/addons

  make $NUMJOBS || make || exit 1
  cp -r build/depends/lib/kodi/addons $PKG/usr/lib${LIBDIRSUFFIX}/kodi/
  cp -r build/depends/share/kodi/addons $PKG/usr/share/kodi/
cd ..

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a README.md $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -p -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION-$CODNAM $PKG
