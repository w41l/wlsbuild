service [2345] name:metricbeat \
    pid:metricbeat \
    /usr/sbin/metricbeat --path.config /etc/metricbeat --path.home /usr/share/metricbeat --path.data /var/lib/metricbeat --path.logs /var/log/metricbeat -c metricbeat.yml run \
    -- Elasticsearch metricbeat agent
