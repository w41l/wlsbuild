# Slackware Installer Patches

If you can live with this only working for the installer option 5 "Install from FTP/HTTP server" running in 'full' or 'terse' mode (i.e. no tagfiles), and inaccurate package sizes being displayed for newer packages, then you could try this technique:

- Copy the two attached patch files to a device that can be mounted from the installer before running `setup`.
- Boot the installer and create a directory with `mkdir /mnt/hd`.
- Mount the device with `mount <device> /mnt/hd`.
- Patch INSURL with `patch /usr/lib/setup/INSURL /mnt/hd/<path>/<to>/INSURL.patch`.
- Patch slackinstall with `patch /usr/lib/setup/slackinstall /mnt/hd/<path>/<to>/slackinstall.patch`.
- Unmount the device with `umount /mnt/hd`.
- To be clean, remove the directory with `rmdir /mnt/hd`.
- Run `setup` as normal.

Original post: https://www.linuxquestions.org/questions/slackware-14/requests-for-current-14-2-15-0-a-4175620463/page623.html#post6312870
