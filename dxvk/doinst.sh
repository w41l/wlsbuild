echo "*** Warning: Stop all wine process!
*
** Usage for WINEARCH win32
* cd ~/.wine (or your WINEPREFIX)
* cd drive_c/windows/system32
* for x in /usr/lib/dxvk/*.@DLLSUFFIX@; do rm -f \$(basename \$x .so); ln -s \$x \$(basename \$x .so); done
*
** Usage for WINEARCH win64
* cd ~/.wine (or your WINEPREFIX)
* cd drive_c/windows/system32
* for x in /usr/lib64/dxvk/*.@DLLSUFFIX@; do rm -f \$(basename \$x .so); ln -s \$x \$(basename \$x .so); done
*
** Only for WINEARCH win64
* cd ~/.wine (or your WINEPREFIX)
* cd drive_c/windows/syswow64
* for x in /usr/lib/dxvk/*.@DLLSUFFIX@; do rm -f \$(basename \$x .so); ln -s \$x \$(basename \$x .so); done
*"
