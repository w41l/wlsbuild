# Increase number of file limit for elasticsearch user to minimum 262144
#rlimit nofile 262144

# Use user-specific tmpdir
#ES_TMPDIR=/var/tmp/elasticsearch

# Required env
KBN_HOME=/usr/share/kibana
KBN_PATH_CONF=/etc/kibana
KIB_HOME=/usr/share/kibana
KIB_PATH_CONF=/etc/kibana

service if:elasticsearch [2345] name:kibana \
    @@ES_USER@:@ES_GROUP@ \
    pid:kibana \
    <service/elasticsearch/running> \
    /usr/share/kibana/bin/kibana --config /etc/kibana/kibana.yml --silent \
    -- Kibana WebUI
