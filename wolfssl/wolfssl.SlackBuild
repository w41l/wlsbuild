#!/bin/sh

# Slackware build script for wolfssl

# Copyright 2019-2024 Widya Walesa <pkgs.w41l@walecha.web.id>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=wolfssl
VERSION=${VERSION:-5.7.4}
BUILD=${BUILD:-1}
TAG=${TAG:-_wls}
PKGTYPE=${PKGTYPE:-tlz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE"
  exit 0
fi

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  PLATFORM="X86"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
  PLATFORM="X86"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
  PLATFORM="X64"
elif [ "$ARCH" = "aarch64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  PLATFORM="ARM64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/wlsbuild}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

set -e
trap 'echo "$0 FAILED at line $LINENO!" | tee -a $OUTPUT/error-${PRGNAM}.log' ERR

rm -rf $PKG
mkdir -p $WRK $PKG
cd $WRK
rm -rf $PRGNAM-$VERSION-stable
tar xvf $CWD/$PRGNAM-$VERSION-stable.tar.gz
cd $PRGNAM-$VERSION-stable || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 754 \) -exec chmod 755 {} \+ \
  -o \( -perm 664 \) -exec chmod 644 {} \+

./autogen.sh

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --disable-fastmath \
  --disable-fasthugemath \
  --disable-bump \
  --disable-debug \
  --disable-examples \
  --enable-static=no \
  --enable-shared=yes \
  --enable-opensslextra \
  --enable-fortress \
  --enable-keygen \
  --enable-certgen \
  --enable-distro \
  --build=$ARCH-slackware-linux

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
strip -g $PKG/usr/lib${LIBDIRSUFFIX}/libwolfssl.a

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  AUTHORS COPYING ChangeLog.md INSTALL LICENSING README README.md SCRIPTS-LIST \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

rm -f $PKG/{,usr}/lib{,64}/*.la

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -p -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE

echo "Cleaning up..."
cd $CWD; rm -r $PKG $WRK/$PRGNAM-$VERSION-stable
