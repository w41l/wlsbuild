#!/bin/sh

# Slackware build script for signon-qt5

# Copyright 2019 Widya Walesa <walecha99@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. FURTHERMORE I AM NOT LIABLE IF
# YOUR DATA IS DESTROYED, YOUR HOUSE BURNS DOWN OR YOUR DOG RUNS OFF.

PRGNAM=signond
VERSION=${VERSION:-8.61_ee891bf9}
GITVERSION=ee891bf9fb18edef06002f98120441ad01c34411
BUILD=${BUILD:-1}
TAG=${TAG:-_wls}
PKGTYPE=${PKGTYPE:-tgz}
NUMJOBS=${NUMJOBS:-"-j$(expr $(nproc) \* 3 / 4)"}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/wlsbuild}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -pipe -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -pipe -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -pipe -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf ${PRGNAM}-${GITVERSION}
tar xvf $CWD/${PRGNAM}-${GITVERSION}.tar.bz2
cd ${PRGNAM}-${GITVERSION}

chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

QTMAKEBIN=qmake-qt5
$QTMAKEBIN PREFIX=/usr LIBDIR=/usr/lib${LIBDIRSUFFIX}
make ${NUMJOBS} || make || exit 1
make INSTALL_ROOT=$PKG install

if pkg-config --exists Qt6Core; then
  make distclean || make clean
  cat $CWD/qt6-support-pr36.patch | patch -p1 --verbose || exit 1
  export QT_MAJOR_VERSION=6
  QTMAKEBIN=qmake6
  $QTMAKEBIN PREFIX=/usr LIBDIR=/usr/lib${LIBDIRSUFFIX}
  make ${NUMJOBS} || make || exit 1
  make INSTALL_ROOT=$PKG install
fi

mv $PKG/etc/signond.conf $PKG/etc/signond.conf.new

rm -r $PKG/usr/share/doc

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a COPYING INSTALL NOTES README.md TODO $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -p -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE

echo "Cleaning up build directory"
cd $WRK; rm -rf ${PRGNAM}-${GITVERSION} $PKG
