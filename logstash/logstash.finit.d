# Increase number of file limit for elasticsearch user to minimum 262144
#rlimit nofile 262144

# Use user-specific tmpdir
#ES_TMPDIR=/var/tmp/elasticsearch

# Required env
LOGSTASH_HOME="/usr/share/logstash"

service if:elasticsearch [2345] name:logstash \
    pid:logstash \
    <service/elasticsearch/running> \
    @@ES_USER@:@ES_GROUP@ \
    /usr/share/logstash/bin/logstash --path.settings /etc/logstash \
    -- Elasticsearch Logstash Service
