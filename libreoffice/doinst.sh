if [ -x usr/bin/update-desktop-database -a -d usr/share/applications ]; then
  usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1
fi

if [ -x usr/bin/update-mime-database -a -d usr/share/mime ]; then
  usr/bin/update-mime-database usr/share/mime >/dev/null 2>&1
fi

if [ -x usr/bin/gtk-update-icon-cache -a -d usr/share/icons ]; then
  usr/bin/gtk-update-icon-cache usr/share/icons/hicolor >/dev/null 2>&1
fi

if [ -x usr/bin/fc-cache -a -d usr/share/fonts ]; then
  usr/bin/fc-cache -f -s usr/share/fonts >/dev/null 2>&1
fi

if [ -r /etc/profile.d/libreoffice.sh ]; then
  if ! grep -q 'qt5' /etc/profile.d/libreoffice.sh; then
    echo '#export SAL_USE_VCLPLUGIN=qt5' >>/etc/profile.d/libreoffice.sh
  fi
  if ! grep -q 'kde5' /etc/profile.d/libreoffice.sh; then
    echo '#export SAL_USE_VCLPLUGIN=kde5' >>/etc/profile.d/libreoffice.sh
  fi
  chmod 755 etc/profile.d/libreoffice.sh
fi
if [ -r /etc/profile.d/libreoffice.csh ]; then
  if ! grep -q 'qt5' /etc/profile.d/libreoffice.csh; then
    echo '#setenv SAL_USE_VCLPLUGIN qt5' >>/etc/profile.d/libreoffice.sh
  fi
  if ! grep -q 'kde5' /etc/profile.d/libreoffice.csh; then
    echo '#setenv SAL_USE_VCLPLUGIN kde5' >>/etc/profile.d/libreoffice.sh
  fi
  chmod 755 etc/profile.d/libreoffice.csh
fi
echo "Check /etc/profile.d/libreoffice.{sh,csh} to set global VCLPLUGIN for libreoffice"
