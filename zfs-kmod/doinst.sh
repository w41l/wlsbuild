echo "Running: /sbin/depmod -a @KERN@"
/sbin/depmod -a @KERN@

echo "To enable zfs support on boot (globally):"
echo " echo '/sbin/modprobe zfs' >>/etc/rc.d/rc.modules.local"
echo "OR on specific kernel version:"
echo " echo '/sbin/modprobe zfs' >>/etc/rc.d/rc.modules-@KERN@"
