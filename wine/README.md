# Vulkan-based Adapter for wined3d

Since wine version 4.6, there is a vulkan-based adapter implementation
for wined3d. To build support for this vulkan-based adapter in wine, we
need mingw-w64-gcc and it's dependency libraries. Wine will detect and use
mingw-w64 compiler to build vulkan-based wined3d adapter automatically
if it's available. If mingw-w64 compiler is not available, wine will just skip
vulkan-based support in wined3d.

# mingw-w64 SlackBuild

You can build mingw-w64 packages using my SlackBuild:

https://gitlab.com/w41l/mingw-w64


# mingw-w64 packages

You can download my mingw-w64 packages here:

https://drive.google.com/drive/folders/1M7g46cUialNgUSuLBgtb8w2UoCssT38J

Please read the README.txt before installing my mingw-w64 packages.

# vkd3d

Vkd3d is a 3D graphics library built on top of Vulkan. It has an API very
similar, but not identical, to Direct3D 12. Wine uses vkd3d libraries for its
implementation of Direct3D 12.

# FAudio

Since wine version 4.2 the FAudio implementation land in Wine for improving
the state of XAudio2 support.

# Wine Gecko and MONO

This two wine companion are now splitted into two other packages:
wine-gecko and wine-mono since those two rarely change.

https://drive.google.com/drive/folders/1TeEI1-FOLsN-EN45L73fsIxGC4UJahOA
