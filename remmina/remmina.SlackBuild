#!/bin/bash

# Slackware build script for remmina

# Copyright 2024  Widya Walesa, ID
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PRGNAM=remmina
VERSION=${VERSION:-1.4.33_101_gbaf6d7da1}
GITVERSION=${GITVERSION:-baf6d7da158dba7a1a6e68e1b8dadfda0bdb3de1}
BUILD=${BUILD:-1}
TAG=${TAG:-_wls}
PKGTYPE=${PKGTYPE:-tlz}

FREERDP3=${FREERDP3:-OFF}
AVAHI=${AVAHI:-OFF}
KWALLET=${KWALLET:-ON}
TELEPATHY=${TELEPATHY:-OFF}
KWALLET=${KWALLET:-OFF}
NEWS=${NEWS:-ON}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
  exit 0
fi

WRK=${WRK:-/tmp/wlsbuild}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686 -lz -lssl -lcrypto -lsodium"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686 -lz -lssl -lcrypto -lsodium"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC -lz -lssl -lcrypto -lsodium"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2 -lz -lssl -lcrypto -lsodium"
  LIBDIRSUFFIX=""
fi

# Enable freerdp3 if available
if pkg-config --exists --print-errors freerdp-client3; then
  FREERDP3=ON
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf Remmina-$GITVERSION
tar xvf $CWD/Remmina-$GITVERSION.tar.bz2
cd Remmina-$GITVERSION

chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

mkdir -p build
cd build
  cmake -G Ninja \
    -DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib${LIBDIRSUFFIX} \
    -DCMAKE_INSTALL_MANDIR=/usr/man \
    -DWITH_AVAHI=$AVAHI \
    -DWITH_TELEPATHY=$TELEPATHY \
    -DWITH_VTE=ON \
    -DWITH_APPINDICATOR=ON \
    -DWITH_NEWS=$NEWS \
    -DWITH_WWW=OFF \
    -DWITH_KF5WALLET=$KWALLET \
    -DWITH_FREERDP3=$FREERDP3 \
    -DCMAKE_BUILD_TYPE=Release ..

  ninja || ninja -j1 -v || exit 1
  DESTDIR=$PKG ninja install
cd ..

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  AUTHORS COPYING ChangeLog *.md LICENSE* TRANSLATION \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
