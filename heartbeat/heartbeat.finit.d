service [2345] name:heartbeat \
    pid:heartbeat \
    /usr/sbin/heartbeat --path.config /etc/heartbeat --path.home /usr/share/heartbeat --path.data /var/lib/heartbeat --path.logs /var/log/heartbeat -c heartbeat.yml run \
    -- Elasticsearch heartbeat agent
