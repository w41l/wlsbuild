#!/bin/csh

# Copyright (C) 2024-2025  Widya Walesa
# Distributed under the GNU General Public License, version 2, as
# published by the Free Software Foundation.

# FreeRDP3 PREFIX
setenv FREERDP3_PREFIX @FREERDP3_PREFIX@

# Modify the compilation/linking environment:
setenv PATH ${FREERDP3_PREFIX}/bin:${PATH}
