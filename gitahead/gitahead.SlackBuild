#!/bin/sh

# Slackware build script for gitahead

# Copyright 2019  Widya Walesa <walecha99@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. FURTHERMORE I AM NOT LIABLE IF
# YOUR DATA IS DESTROYED, YOUR HOUSE BURNS DOWN OR YOUR DOG RUNS OFF.

PRGNAM=gitahead
VERSION=${VERSION:-2.6.3}
GITVER=${GITVER:-b774160afab8f6203f2e3eef9c198c45eaf2baa4}
BUILD=${BUILD:-1}
TAG=${TAG:-_wls}

CWD=$(pwd)
WRK=${WRK:-/tmp/wlsbuild}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}
DOCS="LICENSE.md README.md"

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -pipe -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -pipe -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -pipe -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf $PRGNAM-$VERSION
git clone https://github.com/gitahead/gitahead.git $PRGNAM-$VERSION
cd $PRGNAM-$VERSION
git reset --hard $GITVER
git submodule init
git submodule update

chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

mkdir -p build/release
cd build/release
  cmake -G Ninja \
    -DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS_RELEASE:STRING="$SLKCFLAGS" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr/share/$PRGNAM \
    -DBUILD_SHARED_LIBS=ON \
    ../..

ninja || ninja -j1 -v || exit 1

# Delete unused line from cmake_install.cmake
sed '/libssl.so.1.1/d' -i pack/cmake_install.cmake
sed '/libcrypto.so.1.1/d' -i pack/cmake_install.cmake

ninja package || exit 1
cd ../..

mkdir -p $PKG/usr/share/$PRGNAM
cp -r build/release/_CPack_Packages/Linux/STGZ/GitAhead-${VERSION}/* ${PKG}/usr/share/$PRGNAM/

rm -r $PKG/usr/share/$PRGNAM/*.so.*

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/bin
( cd $PKG/usr/bin; ln -s ../share/gitahead/GitAhead gitahead; )

for i in 128 16 256 32 512 64; do
  mkdir -p $PKG/usr/share/icons/hicolor/${i}x${i};
  cp $PKG/usr/share/$PRGNAM/Resources/GitAhead.iconset/icon_${i}x${i}.png \
    $PKG/usr/share/icons/hicolor/${i}x${i}/gitahead.png;
done
rm -r $PKG/usr/share/$PRGNAM/Resources/GitAhead.iconset/

mkdir -p $PKG/usr/share/applications
cat $CWD/gitahead.desktop >$PKG/usr/share/applications/gitahead.desktop
chmod 644 $PKG/usr/share/applications/gitahead.desktop

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -p -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-${BUILD}${TAG}.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG
